<?php
class Database{
	public $conn;
	private $servername = "localhost";
	private $username = "root";
	private $password = "";
	private $dbname = "smartcommute";

	function connect(){
		$this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		
		
		if ($this->conn->connect_errno)
		  {
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		  }

		// Check if server is alive
		else if ($this->conn->ping())
		  {

		  }
		else
		  {
		  echo "Error: ". mysqli_error($this->conn);
		  }
	}
	
	function close(){
		$this->conn->close();
	}
}

?>